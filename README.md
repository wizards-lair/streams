# (cons 'scheme 'lazy-lists)

This is an implementation of a stream data structure for R7RS Scheme, it follows the Scheme Request For Implementation 41 Specification by Philip L. Bewig but is adapted for the seventh report, some procedures don't raise errors specified in the SRFI and `stream-match` isn't implemented.

For more information about this library check out the spec [here](http://http://srfi.schemers.org/srfi-41/srfi-41.html).

## (streams primitive)

Exports:
- `stream-null`
- `stream-cons`
- `stream?`
- `stream-null?`
- `stream-pair?`
- `stream-car`
- `stream-cdr`
- `stream-lambda`

## (streams derived)

Exports:
- `define-stream`
- `list->stream`
- `port->stream`
- `stream`
- `stream->list`
- `stream-append`
- `stream-concat`
- `stream-constant`
- `stream-drop`
- `stream-drop-while`
- `stream-filter`
- `stream-fold`
- `stream-for-each`
- `stream-from`
- `stream-iterate`
- `stream-length`
- `stream-let`
- `stream-map`
- `stream-of`
- `stream-range`
- `stream-ref`
- `stream-reverse`
- `stream-scan`
- `stream-take`
- `stream-take-while`
- `stream-unfold`
- `stream-unfolds`
- `stream-zip`

## (streams)

Exports all the names in `(streams primitive)` and `(streams derived)`.
